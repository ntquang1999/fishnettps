using FishNet.Transporting.Tugboat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalClientStarter : MonoBehaviour
{
    [SerializeField] private GameObject playfabClientPrefab;
    private Tugboat tugboat;
    private NetworkHudCanvases networkHudCanvas;


    private void Awake()
    {
        tugboat = FindObjectOfType<Tugboat>();
        networkHudCanvas = FindObjectOfType<NetworkHudCanvases>();

        if (PlayerLobbyData.Instance.startType == LobbyStartType.Create)
        {
            Instantiate(playfabClientPrefab);
        }

        if (PlayerLobbyData.Instance.startType == LobbyStartType.Join)
        {
            tugboat.SetClientAddress(PlayerLobbyData.Instance.ServerIP);
            tugboat.SetPort(PlayerLobbyData.Instance.port);
            networkHudCanvas.OnClick_Client();
        }

        if (PlayerLobbyData.Instance.startType == LobbyStartType.LocalDebug)
        {
            Destroy(gameObject);
        }
    }
}
