using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyController : MonoBehaviour
{
    [SerializeField] private TMP_InputField playerNameBox;
    [SerializeField] private TMP_InputField serverIpBox;
    [SerializeField] private TMP_InputField portBox;
    [SerializeField] private TextMeshProUGUI errorMessage;
    [SerializeField] private GameObject errorBox;

    public void CreateMatch()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        PlayerLobbyData.Instance.startType = LobbyStartType.Create;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        SceneManager.LoadScene(1);
    }

    public void JoinMatch()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        if (serverIpBox.text == "")
        {
            ShowError("Please enter Server IP Address!");
            return;
        }

        if (portBox.text == "")
        {
            ShowError("Please enter Server's port!");
            return;
        }

        PlayerLobbyData.Instance.startType = LobbyStartType.Join;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        PlayerLobbyData.Instance.ServerIP = serverIpBox.text;
        PlayerLobbyData.Instance.port = (ushort)Int32.Parse(portBox.text);
        SceneManager.LoadScene(1);
    }

    public void PlayLocal()
    {
        if (playerNameBox.text == "")
        {
            ShowError("Please enter player name!");
            return;
        }

        PlayerLobbyData.Instance.startType = LobbyStartType.LocalDebug;
        PlayerLobbyData.Instance.playerName = playerNameBox.text;
        SceneManager.LoadScene(1);
    }

    private void ShowError(string message)
    {
        errorMessage.text = message;
        errorBox.SetActive(true);
    }
}
