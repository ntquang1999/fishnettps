using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet;
using FishNet.Object;

public class Ball : NetworkBehaviour
{
    public override void OnStartClient()
    {
        base.OnStartClient();
        if(base.IsClientOnly)
        {
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<SphereCollider>());
        }
    }
}
