using FishNet.Managing;
using FishNet.Transporting.Tugboat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.MultiplayerAgent.Model;
using System;
using System.Linq;

public class PlayfabServer : MonoBehaviour
{
    private NetworkManager _networkManager;
    private Tugboat _tugboat;
    private GameManager _gameManager;

    private void Awake()
    {
        _networkManager = FindObjectOfType<NetworkManager>();
        _tugboat = FindObjectOfType<Tugboat>();
        _gameManager = FindObjectOfType<GameManager>();
    }

    private void Start()
    {
        PlayFabMultiplayerAgentAPI.Start();
        PlayFabMultiplayerAgentAPI.OnMaintenanceCallback += OnMaintenance;
        PlayFabMultiplayerAgentAPI.OnShutDownCallback += OnShutdown;
        PlayFabMultiplayerAgentAPI.OnServerActiveCallback += OnServerActive;
        PlayFabMultiplayerAgentAPI.OnAgentErrorCallback += OnAgentError;
        _gameManager.LastPlayerLeaves += OnShutdown;

        var connInfo = PlayFabMultiplayerAgentAPI.GetGameServerConnectionInfo();
        // make sure the ListeningPortKey is the same as the one configured in your Build settings (either on LocalMultiplayerAgent or on MPS)
        const string ListeningPortKey = "game_port";
        var portInfo = connInfo.GamePortsConfiguration.Where(x => x.Name == ListeningPortKey);
        if (portInfo.Count() > 0)
        {
            Debug.Log(string.Format("port with name {0} was found in GSDK Config Settings.", ListeningPortKey));
            _tugboat.SetPort((ushort)portInfo.Single().ServerListeningPort);
        }
        else
        {
            string msg = string.Format("Cannot find port with name {0} in GSDK Config Settings. Please make sure the LocalMultiplayerAgent is running and that the MultiplayerSettings.json file includes correct name as a GamePort Name.", ListeningPortKey);
            Debug.LogError(msg);
        }

        StartCoroutine(ReadyForPlayers());
    }

    IEnumerator ReadyForPlayers()
    {
        yield return new WaitForSeconds(.5f);
        PlayFabMultiplayerAgentAPI.ReadyForPlayers();
    }

    private void OnServerActive()
    {
        _networkManager.ServerManager.StartConnection();
        Debug.Log("Server Started From Agent Activation");
    }

    private void OnAgentError(string error)
    {
        Debug.Log(error);
    }

    private void OnShutdown()
    {
        Debug.Log("Server is shutting down");
        StartCoroutine(Shutdown());
    }

    IEnumerator Shutdown()
    {
        yield return new WaitForSeconds(5f);
        Application.Quit();
    }

    private void OnMaintenance(DateTime? NextScheduledMaintenanceUtc)
    {
        Debug.LogFormat("Maintenance scheduled for: {0}", NextScheduledMaintenanceUtc.Value.ToLongDateString());
    }
}
