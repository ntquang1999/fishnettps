using UnityEngine;
using UnityEngine.InputSystem;


public class InputManager : MonoBehaviour
{
	[Header("Character Input Values")]
	public Vector2 move;
	public Vector2 look;
	public bool jump;
	public bool sprint;
	public bool aim;
	public bool skill_1;
	public bool skill_2;
	public bool skill_3;
	public bool mainAction;
	public bool throwAction;
	public bool forceShowCursor = false;

	[Header("Movement Settings")]
	public bool analogMovement;

	[Header("Mouse Cursor Settings")]
	public bool cursorLocked = false;
	public bool cursorInputForLook = true;

	public void OnMove(InputValue value)
	{
		MoveInput(value.Get<Vector2>());
	}

	public void OnLook(InputValue value)
	{
		if (cursorInputForLook)
		{
			LookInput(value.Get<Vector2>());
		}
	}

	public void OnJump(InputValue value)
	{
		JumpInput(value.isPressed);
	}

	public void OnSprint(InputValue value)
	{
		SprintInput(value.isPressed);
	}

	public void OnAim(InputValue value)
	{
		AimInput(value.isPressed);
	}

	public void OnSkill_1(InputValue value)
	{
		Skill_1_Input(value.isPressed);
	}

	public void OnSkill_2(InputValue value)
	{
		Skill_2_Input(value.isPressed);
	}

	public void OnSkill_3(InputValue value)
	{
		Skill_3_Input(value.isPressed);
	}

	public void OnMainAction(InputValue value)
	{
		MainActionInput(value.isPressed);
	}
	public void OnThrowAction(InputValue value)
    {
		ThrowInput(value.isPressed);
    }

	public void OnSwitchCusor(InputValue value)
	{
		SwitchCusorState();
	}


	public void MoveInput(Vector2 newMoveDirection)
	{
		move = newMoveDirection;
	}

	public void LookInput(Vector2 newLookDirection)
	{
		look = newLookDirection;
	}

	public void JumpInput(bool newJumpState)
	{
		jump = newJumpState;
	}

	public void SprintInput(bool newSprintState)
	{
		sprint = newSprintState;
	}

	public void AimInput(bool newAimState)
	{
		aim = newAimState;
	}

	public void Skill_1_Input(bool newSkill1State)
	{
		skill_1 = newSkill1State;
	}

	public void Skill_2_Input(bool newSkill2State)
	{
		skill_2 = newSkill2State;
	}

	public void Skill_3_Input(bool newSkill3State)
	{
		skill_3 = newSkill3State;
	}

	public void MainActionInput(bool newMainState)
	{
		mainAction = newMainState;
	}

	public void ThrowInput(bool newThrowState)
    {
		throwAction = newThrowState;
    }

	public void SwitchCusorState()
	{
		if (forceShowCursor)
		{
			Cursor.lockState = CursorLockMode.None;
			return;
		}

		if (cursorLocked)
			Cursor.lockState = CursorLockMode.None;
		else
			Cursor.lockState = CursorLockMode.Locked;
		cursorLocked = !cursorLocked;
	}

	public void SetCursorState(bool newState)
	{
		Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
	}
}