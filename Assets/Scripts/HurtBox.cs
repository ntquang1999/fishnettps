using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    public float Multiplier;
    public event Action<float> hit;


    public void Hit(float weaponDamage)
    {
        hit?.Invoke(Multiplier * weaponDamage);
    }
}
