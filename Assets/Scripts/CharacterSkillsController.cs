using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterSkillsController : NetworkBehaviour
{
    [SerializeField] public Skill skill_1;
    [SerializeField] public Skill skill_2;
    [SerializeField] public Skill skill_3;

    public string keybindSkill_1 = "E";
    public string keybindSkill_2 = "Q";
    public string keybindSkill_3 = "C";
    public List<int> Skills = new List<int>();

    private void Awake()
    {
        skill_1 = AddSkill(Skills[0]);
        skill_2 = AddSkill(Skills[1]);
        skill_3 = AddSkill(Skills[2]);
    }

    private Skill AddSkill(int index)
    {
        switch(index)
        {
            case 0:
                return gameObject.AddComponent<Dash>();
            case 1:
                return gameObject.AddComponent<FlyingKick>();
            case 2:
                return gameObject.AddComponent<PowerUp>();
            default:
                return null;
        }
    }

    public void ProcessSkillInput(InputData inputData, float deltaTime)
    {
        if (inputData.Skill_1 && skill_1.GetCooldown() <= 0f)
        {
            EndActionSkills();
        }

        if (inputData.Skill_2 && skill_2.GetCooldown() <= 0f)
        {
            EndActionSkills();
        }

        if (inputData.Skill_3 && skill_3.GetCooldown() <= 0f)
        {
            EndActionSkills();
        }

        skill_1.ReduceCooldown(deltaTime);
        skill_2.ReduceCooldown(deltaTime);
        skill_3.ReduceCooldown(deltaTime);
        skill_1.PerformSkill(inputData, deltaTime);
        skill_2.PerformSkill(inputData, deltaTime);
        skill_3.PerformSkill(inputData, deltaTime);
    }

    private void EndActionSkills()
    {
        if (skill_1.type == SkillType.Action && skill_1.isPerforming) skill_1.EndSkill();
        if (skill_2.type == SkillType.Action && skill_2.isPerforming) skill_2.EndSkill();
        if (skill_3.type == SkillType.Action && skill_3.isPerforming) skill_3.EndSkill();
    }
}
