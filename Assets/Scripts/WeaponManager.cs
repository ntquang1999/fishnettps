using FishNet.Object;
using FishNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object.Synchronizing;
using FishNet.Object.Prediction;
using System;
using FishNet.Managing.Timing;

public class WeaponManager : NetworkBehaviour
{
    [SerializeField] GameObject gunBack;
    [SerializeField] GameObject gunHand;
    private InputManager _input;
    private NetworkCharacterController _networkCharacterController;
    private Health health;
    private Camera mainCamera;
    private Transform gunExitPoint;
    private float baseFireDelay = 0.2f;
    private float fireDelay = 0f;

    public GameObject hitIndicator;
    public TrailRenderer bulletTrail;
    public GameObject ballPrefab;
    public event Action<bool> Aiming;


    private void Awake()
    {
        InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;
        _input = GetComponent<InputManager>();
        _networkCharacterController = GetComponent<NetworkCharacterController>();
        mainCamera = Camera.main;
        gunExitPoint = gunHand.GetComponent<Weapon>().ExitPoint;
        health = GetComponent<Health>();
    }

    private void TimeManager_OnTick()
    {
        fireDelay -= (float)base.TimeManager.TickDelta;
    }

    private void Update()
    {
        if (!base.IsOwner) return;

        HandleWeapon();
        
        if(_networkCharacterController.characterStates.isAiming && _input.mainAction && fireDelay <= 0f)
        {
            Vector3 aimPoint = GetAimPoint();
            FireWeapon(base.TimeManager.GetPreciseTick(TickType.Tick), gunExitPoint.position, aimPoint - gunExitPoint.position, aimPoint);
        }

        if(_networkCharacterController.characterStates.isAiming && _input.throwAction && fireDelay <= 0f)
        {
            ThrowBall(gunExitPoint.position);
        }

    }

    private void HandleWeapon()
    {
        if (!_networkCharacterController.characterStates.isControllable)
        {
            SetAimState(false);
            Aiming?.Invoke(false);
            return;
        }

        if (_networkCharacterController.characterStates.isAiming)
        {
            SetAimState(true);
            Aiming?.Invoke(true);
        }
        else
        {
            SetAimState(false);
            Aiming?.Invoke(false);
        }
    }

    [ServerRpc]
    public void ThrowBall(Vector3 startPos)
    {
        if (fireDelay > 0f) return;
        fireDelay = baseFireDelay;
        GameObject ball = Instantiate(ballPrefab, startPos, Quaternion.identity);
        base.Spawn(ball, null);
        ball.GetComponent<Rigidbody>().AddForce(transform.forward * 120f);
    }

    [ServerRpc(RunLocally = true)]
    private void FireWeapon(PreciseTick pt, Vector3 position, Vector3 forward, Vector3 aimPoint)
    {
        if (fireDelay > 0f) return;
        fireDelay = baseFireDelay;
        //_networkCharacterController.ThrowBall(gunExitPoint.position);
        //return;
        Ray shootingRay = new Ray(position, forward);
        bool enableRollback = base.IsServer;

        if(enableRollback)
            RollbackManager.Rollback(pt, FishNet.Component.ColliderRollback.RollbackManager.PhysicsType.ThreeDimensional);

        health.SetIgnoreRaycast(true);

        if (Physics.Raycast(shootingRay, out RaycastHit hit, float.PositiveInfinity))
        {
            ShootingEffect(position, hit.point);
            ShootClient(position, hit.point);
            if (base.IsClient)
                if(hit.collider.gameObject.CompareTag("Enemy"))
                {
                    Debug.Log("Client hit Enemy");
                }

            //Apply damage and other server things.
            if (base.IsServer)
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    hit.collider.gameObject.GetComponent<HurtBox>().Hit(10);
                }
                GameObject r = Instantiate(hitIndicator, hit.point, Quaternion.identity);
                base.Spawn(r, null);
            }
        }
        else
        {
            ShootingEffect(position, aimPoint);
        }

        health.SetIgnoreRaycast(false);

        if (enableRollback)
            RollbackManager.Return();

    }

    [ObserversRpc]
    private void DebugFromServer(string message)
    {
        Debug.Log(message);
    }

    [ObserversRpc]
    private void ShootClient(Vector3 ShootPosition, Vector3 hitPosition)
    {
        if(!base.IsOwner)
        {
            ShootingEffect(ShootPosition, hitPosition);
        }
    }
    private void ShootingEffect(Vector3 ShootPosition, Vector3 hitPosition)
    {
        if (base.IsServerOnly) return;
        TrailRenderer trail = Instantiate(bulletTrail, ShootPosition, Quaternion.identity);
        StartCoroutine(SpawnTrail(trail, hitPosition));
    }

    IEnumerator SpawnTrail(TrailRenderer trail, Vector3 hitPosition)
    {
        float time = 0f;
        while(time < 1)
        {
            trail.transform.position = Vector3.Lerp(trail.transform.position, hitPosition, time);
            time += Time.deltaTime / trail.time;

            yield return null;
        }

        Destroy(trail.gameObject, trail.time);
    }

    private Vector3 GetAimPoint()
    {
        Vector2 centerScreenPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = mainCamera.ScreenPointToRay(centerScreenPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f))
        {
            return raycastHit.point;
        }
        else
        {
            return mainCamera.transform.position + mainCamera.transform.forward * 200f;
        }
    }


    [ServerRpc]
    public void SetAimState(bool state)
    {
        SetGunState(state);
    }

    [ObserversRpc(RunLocally = true)]
    public void SetGunState(bool aim)
    {
        gunBack.SetActive(!aim);
        gunHand.SetActive(aim);
    }


}
