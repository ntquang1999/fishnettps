using FishNet.Component.Animating;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillType
{
    Action,
    Buff,
}

public abstract class Skill : MonoBehaviour
{
    public bool isPerforming = false;
    public SkillType type;
    public Sprite icon;

    protected Animator animator;
    protected NetworkAnimator networkAnimator;
    protected NetworkCharacterController networkCharacterController;
    protected CharacterController characterController;
    

    protected float skillCooldown = 0f;
    protected float skillDuration = 0f;
    protected int animID;
    [SerializeField] protected bool coolDownOnCast = true;

    protected void Awake()
    {
        animator = GetComponent<Animator>();
        networkAnimator = GetComponent<NetworkAnimator>();
        networkCharacterController = GetComponent<NetworkCharacterController>();
        characterController = GetComponent<CharacterController>();
        SetAnimationID();
        SetIcon();
        SetType();
    }

    protected abstract void SetAnimationID();
    protected abstract void SetIcon();

    protected abstract void SetType();
    public abstract void PerformSkill(InputData inputData, float deltaTime);
    public abstract void EndSkill();

    public void ReduceDuration(float deltaTime)
    {
        if (skillDuration > 0f)
            skillDuration -= deltaTime;
        else
            skillDuration = 0f;
    }

    public void ReduceCooldown(float deltaTime)
    {
        if (!coolDownOnCast && skillDuration > 0f) return;

        if (skillCooldown > 0f)
            skillCooldown -= deltaTime;
        else
            skillCooldown = 0f;
    }

    public void SetDuration(float time)
    {
        skillDuration = time;
    }

    public void SetCooldown(float time)
    {
        skillCooldown = time;
    }

    public float GetDuration()
    {
        return skillDuration;
    }

    public float GetCooldown()
    {
        return skillCooldown;
    }

}
