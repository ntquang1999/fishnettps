using FishNet;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientPlayerSpawner : NetworkBehaviour
{
    [Tooltip("Character prefab to spawn.")]
    [SerializeField]
    private GameObject _characterPrefab;

    [SerializeField]
    private GameObject _ballPrefab;

    [SerializeField]
    private GameObject _clientCanvas;

    public GameObject LocalPlayer;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            CmdSpawnPlayer();
        }
        else
        {
            _clientCanvas.SetActive(false);
        }
    }

    [ServerRpc]
    public void CmdSpawnPlayer()
    {
        SpawnPlayer();
    }

    public void SpawnPlayer()
    {
        GameObject player = Instantiate(_characterPrefab, SpawnPoint.Instance.GetRandomSpawnPoint(), Quaternion.identity);
        base.Spawn(player, base.Owner);
        LocalPlayer = player;
        LocalPlayer.GetComponent<Health>().Die += OnDie;
        OnSpawnClient();
    }

    private void OnDie()
    {
        OnDieClient();
    }

    [ObserversRpc]
    private void OnSpawnClient()
    {
        if(base.IsOwner)
            _clientCanvas.SetActive(false);
    }

    [ObserversRpc]
    private void OnDieClient()
    {
        if(base.IsOwner)
            _clientCanvas.SetActive(true);
    }

    [ServerRpc]
    public void Respawn()
    {
        InstanceFinder.ServerManager.Despawn(LocalPlayer);
        SpawnPlayer();
    }
}
