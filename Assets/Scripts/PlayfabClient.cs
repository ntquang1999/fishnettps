using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.MultiplayerModels;
using PlayFab.ClientModels;
using PlayFab;
using FishNet.Transporting.Tugboat;

public class PlayfabClient : MonoBehaviour
{
    private Tugboat _tugboat;
    private NetworkHudCanvases _networkHudCanvas;

    private void Awake()
    {
        _tugboat = FindObjectOfType<Tugboat>();
        _networkHudCanvas = FindObjectOfType<NetworkHudCanvases>();
    }

    void Start()
    {
        var login = new LoginWithPlayFabRequest
        { Username = "ntquang1999", Password = "Quang24041999" };
        PlayFabClientAPI.LoginWithPlayFab(login, OnLoginSuccess, OnLoginFailure);
    }

    private void RequestMultiplayerServer()
    {
        RequestMultiplayerServerRequest requestData = new RequestMultiplayerServerRequest();
        requestData.BuildId = "08fdef40-c497-4d53-a69a-2d18a20bb563"; // Build ID from the Multiplayer Dashboard
        requestData.PreferredRegions = new List<string>() { "SoutheastAsia" };
        requestData.SessionId = System.Guid.NewGuid().ToString(); // Generate a Session ID
        PlayFabMultiplayerAPI.RequestMultiplayerServer(requestData, OnRequestMultiplayerServer, OnRequestMultiplayerServerError);
    }

    private void OnRequestMultiplayerServer(RequestMultiplayerServerResponse response)
    {
        Debug.Log("Requested: " + response.IPV4Address + ":" + response.Ports[0].Num);
        _tugboat.SetClientAddress(response.IPV4Address);
        _tugboat.SetPort((ushort)response.Ports[0].Num);
        _networkHudCanvas.OnClick_Client();
    }

    private void OnRequestMultiplayerServerError(PlayFabError error)
    {
        Debug.Log(error.ErrorMessage);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Logged in");
        RequestMultiplayerServer();
    }

    private void OnLoginFailure(PlayFabError error)
    {
        if (error.ErrorDetails != null && error.ErrorDetails.Count > 0)
        {
            using (var iter = error.ErrorDetails.Keys.GetEnumerator())
            {
                iter.MoveNext();
                string key = iter.Current;
                Debug.LogError(error.ErrorDetails[key][0]);
            }
        }
        else
        {
            Debug.LogError(error.ErrorMessage);
        }
    }
}
