using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PersonalUIManager : NetworkBehaviour
{
    [SerializeField] CharacterSkillsController skillsController;

    [SerializeField] private TextMeshProUGUI cooldownSkill_1;
    [SerializeField] private Image iconSkill_1;
    [SerializeField] private TextMeshProUGUI keybindSkill_1;

    [SerializeField] private TextMeshProUGUI cooldownSkill_2;
    [SerializeField] private Image iconSkill_2;
    [SerializeField] private TextMeshProUGUI keybindSkill_2;

    [SerializeField] private TextMeshProUGUI cooldownSkill_3;
    [SerializeField] private Image iconSkill_3;
    [SerializeField] private TextMeshProUGUI keybindSkill_3;

    [SerializeField] private GameObject crosshair;
    [SerializeField] private TextMeshProUGUI healthText;

    [SerializeField] WeaponManager weaponManager;
    [SerializeField] Health health;

    private void Awake()
    {
        weaponManager.Aiming += OnAiming;
        health.HealthUpdate += OnHealthChanged;
    }

    private void OnDisable()
    {
        weaponManager.Aiming -= OnAiming;
        health.HealthUpdate -= OnHealthChanged;
    }

    public void OnAiming(bool aiming)
    {
        crosshair.SetActive(aiming);
    }

    public void OnHealthChanged(float health)
    {
        healthText.text = health.ToString("0");
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner) gameObject.SetActive(false);
        iconSkill_1.sprite = skillsController.skill_1.icon;
        iconSkill_2.sprite = skillsController.skill_2.icon;
        iconSkill_3.sprite = skillsController.skill_3.icon;
        keybindSkill_1.text = skillsController.keybindSkill_1;
        keybindSkill_2.text = skillsController.keybindSkill_2;
        keybindSkill_3.text = skillsController.keybindSkill_3;
    }

    private void Update()
    {
        cooldownSkill_1.text = GetCooldownText(skillsController.skill_1.GetCooldown());
        cooldownSkill_2.text = GetCooldownText(skillsController.skill_2.GetCooldown());
        cooldownSkill_3.text = GetCooldownText(skillsController.skill_3.GetCooldown());
    }

    private string GetCooldownText(float cooldown)
    {
        if (cooldown > 0f)
            return cooldown.ToString("0.0");
        else
            return "Ready";
    }
}
