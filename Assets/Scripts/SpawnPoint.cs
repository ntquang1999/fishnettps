using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public static SpawnPoint Instance;

    private void Awake()
    {
        Instance = this;
    }

    public Vector3 GetRandomSpawnPoint()
    {
        return new Vector3(Random.Range(transform.position.x - 5, transform.position.x +5), transform.position.y, Random.Range(transform.position.z - 5, transform.position.z + 5));
    }

}
