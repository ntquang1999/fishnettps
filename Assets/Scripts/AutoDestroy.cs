using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : NetworkBehaviour
{
    public float time = 0.5f;
    public override void OnStartServer()
    {
        base.OnStartServer();
        StartCoroutine(AutoDestroyCoroutine(time));
    }
    IEnumerator AutoDestroyCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        base.Despawn();
    }
}
