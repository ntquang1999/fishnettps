using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingKick : Skill
{
    [SerializeField] private float BaseSkillDuration = 1.1f;
    [SerializeField] private float BaseSkillCooldown = 5f;
    [SerializeField] private string animationName = "FlyingKick";
    protected override void SetAnimationID()
    {
        animID = Animator.StringToHash(animationName);
    }

    protected override void SetIcon()
    {
        icon = Resources.Load<Sprite>("flyingkick");
    }

    protected override void SetType()
    {
        type = SkillType.Action;
    }

    public override void PerformSkill(InputData inputData, float deltaTime)
    {

        if (inputData.Skill_2 && skillCooldown <= 0f && networkCharacterController.characterStates.isControllable)
        {
            animator.SetBool(animID, true);
            skillDuration = BaseSkillDuration;
            networkCharacterController.characterStates.isControllable = false;
            networkCharacterController.characterStates.isPerformingSkill = true;
            skillCooldown = BaseSkillCooldown;
            isPerforming = true;
        }

        skillDuration -= deltaTime;

        if (!isPerforming) return;

        if (skillDuration < 0f)
        {
            EndSkill();
        }
        else if (skillDuration > 0f && (skillDuration < 0.75f|| !networkCharacterController.Grounded))
        {
            moveCharacter(inputData, deltaTime);
        }
    }

    public override void EndSkill()
    {
        animator.SetBool(animID, false);
        networkCharacterController.characterStates.isControllable = true;
        networkCharacterController.characterStates.isPerformingSkill = false;
        isPerforming = false;
    }

    private void moveCharacter(InputData inputData, float deltaTime)
    {
        float speed = 4f;
        if (!networkCharacterController.Grounded && inputData.Sprint) speed = 4f + networkCharacterController.SprintSpeed - networkCharacterController.MoveSpeed;
        characterController.Move(transform.forward.normalized * (speed * deltaTime) +
                         new Vector3(0.0f, networkCharacterController._verticalVelocity, 0.0f) * deltaTime);
    }
}
