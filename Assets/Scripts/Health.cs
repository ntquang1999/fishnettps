using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;

public class Health : NetworkBehaviour
{
    [SyncVar(OnChange = nameof(OnHealthChanged))]
    private float health;

    public event Action<float> HealthUpdate;
    public event Action Die;
    private HurtBox[] hurtBoxes;

    private void Awake()
    {
        hurtBoxes = GetComponentsInChildren<HurtBox>();
    }
    public override void OnStartNetwork()
    {
        base.OnStartNetwork();
        health = 100;
        foreach(HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit += ReduceHealth;
        }
    }

    public void OnDisable()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit -= ReduceHealth;
        }
    }

    public void ReduceHealth(float amount)
    {
        health -= amount;
        if (health <= 0)
        {
            health = 0;
            Die?.Invoke();
        }
    }

    private void OnHealthChanged(float prev, float next, bool asServer)
    {
        HealthUpdate?.Invoke(next);
        if(health <= 0) Die?.Invoke();
    }

    public void SetIgnoreRaycast(bool isIgnore)
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            if (isIgnore) hurtBox.gameObject.layer = 2;
            else hurtBox.gameObject.layer = 3;
        }
    }
}
