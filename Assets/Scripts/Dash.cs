using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Skill
{
    [SerializeField] private float BaseSkillDuration = 0.15f;
    [SerializeField] private float BaseSkillCooldown = 3f;
    [SerializeField] private string animationName = "Dashing";
    private Vector3 direction;
    protected override void SetAnimationID()
    {
        animID = Animator.StringToHash(animationName);
    }

    protected override void SetIcon()
    {
        icon = Resources.Load<Sprite>("dash");
    }

    protected override void SetType()
    {
        type = SkillType.Action;
    }

    public override void PerformSkill(InputData inputData, float deltaTime)
    {

        if (inputData.Skill_1 && skillCooldown <= 0f && networkCharacterController.characterStates.isControllable)
        {
            animator.SetBool(animID, true);
            skillDuration = BaseSkillDuration;
            networkCharacterController.characterStates.isControllable = false;
            networkCharacterController.characterStates.isPerformingSkill = true;
            skillCooldown = BaseSkillCooldown;
            isPerforming = true;
            if (inputData.Move == Vector2.zero || !networkCharacterController.characterStates.isAiming)
                direction = transform.forward.normalized;
            else
            {
                if(inputData.Move == Vector2.left)
                {
                    direction = -transform.right.normalized;
                }
                if (inputData.Move == Vector2.right)
                {
                    direction = transform.right.normalized;
                }
                if (inputData.Move == Vector2.up)
                {
                    direction = transform.forward.normalized;
                }
                if (inputData.Move == Vector2.down)
                {
                    direction = -transform.forward.normalized;
                }
            }
        }
        
        skillDuration -= deltaTime;

        if (!isPerforming) return;

        if (skillDuration < 0f)
        {
            EndSkill();
        }
        else if (skillDuration > 0f )
        {
            moveCharacter(deltaTime);
        }
    }

    public override void EndSkill()
    {
        animator.SetBool(animID, false);
        networkCharacterController.characterStates.isControllable = true;
        networkCharacterController.characterStates.isPerformingSkill = false;
        isPerforming = false;
    }

    private void moveCharacter(float deltaTime)
    {
        characterController.Move(direction * (40f * deltaTime) +
                         new Vector3(0.0f, networkCharacterController._verticalVelocity, 0.0f) * deltaTime);
    }    
}
