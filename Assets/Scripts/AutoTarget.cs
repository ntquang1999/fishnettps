using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTarget : NetworkBehaviour
{
    [SerializeField]
    private LayerMask aimLayer = new LayerMask();
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (!base.IsOwner) return;
        Vector2 centerScreenPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = mainCamera.ScreenPointToRay(centerScreenPoint);
        if(Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimLayer))
        {
            transform.position = raycastHit.point;
        }
        else
        {
            transform.position = mainCamera.transform.position + mainCamera.transform.forward * 200f;
        }
    }
}
