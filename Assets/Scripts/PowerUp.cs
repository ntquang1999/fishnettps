using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Skill
{
    [SerializeField] private float BaseSkillDuration = 10f;
    [SerializeField] private float BaseSkillCooldown = 20f;
    [SerializeField] private string animationName = "PowerUp";

    private bool isTriggered;
    protected override void SetAnimationID()
    {
        animID = Animator.StringToHash(animationName);
    }

    protected override void SetIcon()
    {
        icon = Resources.Load<Sprite>("power");
    }

    protected override void SetType()
    {
        type = SkillType.Buff;
    }

    public override void PerformSkill(InputData inputData, float deltaTime)
    {

        if (inputData.Skill_3 && skillCooldown <= 0f && networkCharacterController.characterStates.isControllable)
        {
            animator.SetBool(animID, true);
            skillDuration = BaseSkillDuration;
            networkCharacterController.characterStates.isControllable = false;
            networkCharacterController.characterStates.isPerformingSkill = true;
            skillCooldown = BaseSkillCooldown;
            isPerforming = true;
            isTriggered = false;
        }
        
        skillDuration -= deltaTime;

        if (!isPerforming) return;

        if (skillDuration < 0f)
        {
            EndSkill();
        }
        else if (skillDuration > 0f && skillDuration < 8f && !isTriggered)
        {
            isTriggered = true;
            animator.SetBool(animID, false);
            networkCharacterController.characterStates.isControllable = true;
            networkCharacterController.characterStates.isPerformingSkill = false;
            networkCharacterController.MoveSpeed += 3f;
            networkCharacterController.SprintSpeed += 6f;
        }
    }

    public override void EndSkill()
    {
        networkCharacterController.MoveSpeed -= 3f;
        networkCharacterController.SprintSpeed -= 6f;
        isPerforming = false;
    }
}
