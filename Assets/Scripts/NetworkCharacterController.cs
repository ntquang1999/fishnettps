using Cinemachine;
using FishNet;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;
using FishNet.Component.Animating;
using System;
using FishNet.Serializing.Helping;
using System.Collections.Generic;

[Serializable]
public struct CharacterStates
{
    public bool isControllable;
    public bool isMovable;
    public bool isAiming;
    public bool isShooting;
    public bool isPerformingSkill;
    public bool isAlive;

    public CharacterStates(bool isNew)
    {
        isControllable = true;
        isMovable = true;
        isAiming = false;
        isShooting = false;
        isPerformingSkill = false;
        isAlive = true;
    }
}

public struct InputData
{
    public Vector2 Move;
    public bool Jump;
    public float CameraEulerY;
    public bool Sprint;
    public bool Aim;
    public bool Skill_1;
    public bool Skill_2;
    public bool Skill_3;
}

//ReconcileData for Reconciliation
public struct ReconcileData
{
    public Vector3 Position;
    public Quaternion Rotation;
    public float VerticalVelocity;
    public float FallTimeout;
    public float JumpTimeout;
    public bool Grounded;
    public CharacterStates States;
    public float Skill_1_Duration;
    public float Skill_1_Cooldown;
    public float Skill_2_Duration;
    public float Skill_2_Cooldown;
    public float Skill_3_Duration;
    public float Skill_3_Cooldown;
    public float MoveSpeed;
    public float SprintSpeed;

    public ReconcileData(Vector3 position, Quaternion rotation, float verticalVelocity, float fallTimeout, float jumpTimeout, bool grounded, CharacterStates states,
        float skill_1_duration, float skill_1_cooldown,
        float skill_2_duration, float skill_2_cooldown,
        float skill_3_duration, float skill_3_cooldown,
        float moveSpeed, float sprintSpeed)
    {
        Position = position;
        Rotation = rotation;
        VerticalVelocity = verticalVelocity;
        FallTimeout = fallTimeout;
        JumpTimeout = jumpTimeout;
        Grounded = grounded;
        States = states;
        Skill_1_Duration = skill_1_duration;
        Skill_1_Cooldown = skill_1_cooldown;
        Skill_2_Duration = skill_2_duration;
        Skill_2_Cooldown = skill_2_cooldown;
        Skill_3_Duration = skill_3_duration;
        Skill_3_Cooldown = skill_3_cooldown;
        MoveSpeed = moveSpeed;
        SprintSpeed = sprintSpeed;

    }
}

public class NetworkCharacterController : NetworkBehaviour
{
    [Header("Player")]
    [Tooltip("Move speed of the character in m/s")]
    public float MoveSpeed = 2.0f;

    [Tooltip("Sprint speed of the character in m/s")]
    public float SprintSpeed = 6f;


    [Tooltip("Normal Camera Sensitivity")]
    public float NormalSensitivity = 1f;

    [Tooltip("Camera Sensitivity while aiming")]
    public float AimSensitivity = 0.5f;

    [Tooltip("How fast the character turns to face movement direction")]
    [Range(10f, 30f)]
    public float RotationSmoothTime = 12f;

    [Tooltip("Acceleration and deceleration")]
    public float SpeedChangeRate = 10.0f;

    [Space(10)]
    [Tooltip("The height the player can jump")]
    public float JumpHeight = 1.2f;

    [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
    public float Gravity = -15.0f;

    [Space(10)]
    [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    public float JumpTimeout = 0.50f;

    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    public float FallTimeout = 0.15f;

    [Header("Player Grounded")]
    [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
    public bool Grounded = true;

    [Tooltip("Useful for rough ground")]
    public float GroundedOffset = -0.14f;

    [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
    public float GroundedRadius = 0.28f;

    [Tooltip("What layers the character uses as ground")]
    public LayerMask GroundLayers;

    [Header("Cinemachine")]
    [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
    public GameObject CinemachineCameraTarget;

    [Tooltip("How far in degrees can you move the camera up")]
    public float TopClamp = 70.0f;

    [Tooltip("How far in degrees can you move the camera down")]
    public float BottomClamp = -30.0f;

    [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
    public float CameraAngleOverride = 0.0f;

    [Tooltip("For locking the camera position on all axis")]
    public bool LockCameraPosition = false;


    [Tooltip("Set Audio Clip for character")]
    public AudioClip LandingAudioClip;
    public AudioClip[] FootstepAudioClips;
    [Range(0, 1)] public float FootstepAudioVolume = 0.5f;

    [Header("Floating UI")]
    public TextMeshProUGUI playerNameUI;

    

    //Network Variables
    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPlayerSetName))]
    private string playerName;

    [SyncVar(Channel = Channel.Unreliable)]
    private float rigWeight;

    //Public
    [SerializeField]
    public CharacterStates characterStates;

    public GameObject _ballPrefab;

    // cinemachine
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    private float _sensitivity = 1f;

    // player
    public float _animationBlend;
    public float _targetRotation = 0.0f;
    public float _rotationVelocity;
    public float _verticalVelocity;
    public float _terminalVelocity = 53.0f;
    public bool _aiming = true;

    // timeout deltatime
    public float _jumpTimeoutDelta;
    public float _fallTimeoutDelta;

    // animation IDs
    private int _animIDSpeed;
    private int _animIDGrounded;
    private int _animIDJump;
    private int _animIDFreeFall;
    private int _animIDMotionSpeed;
    private int _animIDDirectionX;
    private int _animIDDirectionZ;
    private int _animIDisDead;

    //Components
    private PlayerInput _playerInput;
    private Animator _animator;
    private CharacterController _controller;
    private InputManager _localInput;
    private GameObject _mainCamera;
    private CinemachineVirtualCamera _aimCamera;
    private Rig _rig;
    private NetworkAnimator _networkAnimator;
    private CharacterSkillsController _characterSkillsController;
    private Health _health;


    private const float _threshold = 0.01f;

    private bool _hasAnimator;

    public float FlyingKickTimeout = 0.15f;

    //MoveData for client simulation
    private InputData _clientMoveData;

    //MoveData for replication
    

    #region Initialization
    private bool IsCurrentDeviceMouse
    {
        get
        {
            if (_playerInput == null) return false;
            return _playerInput.currentControlScheme == "KeyboardMouse";
        }
    }

    private void OnPlayerSetName(string prev, string next, bool asServer)
    {
        playerNameUI.text = next;
    }

    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        _animIDDirectionX = Animator.StringToHash("DirectionX");
        _animIDDirectionZ = Animator.StringToHash("DirectionZ");
        _animIDisDead = Animator.StringToHash("isDead");
    }

    private void Awake()
    {
        InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;
        InstanceFinder.TimeManager.OnUpdate += TimeManager_OnUpdate;
        _controller = GetComponent<CharacterController>();
        _aimCamera = FindObjectOfType<VirtualCameraManager>().aimCamera.GetComponent<CinemachineVirtualCamera>();
        _rig = GetComponentInChildren<Rig>();
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        _localInput = GetComponent<InputManager>();
        _networkAnimator = GetComponent<NetworkAnimator>();
        characterStates = new CharacterStates(true);
        _characterSkillsController = GetComponent<CharacterSkillsController>();
        _health = GetComponent<Health>();
        _health.Die += OnDie;
    }

    private void OnDestroy()
    {
        if (InstanceFinder.TimeManager != null)
        {
            InstanceFinder.TimeManager.OnTick -= TimeManager_OnTick;
            InstanceFinder.TimeManager.OnUpdate -= TimeManager_OnUpdate;
        }
        _health.Die -= OnDie;
    }


    public override void OnStartClient()
    {
        base.OnStartClient();

        _controller.enabled = (base.IsServer || base.IsOwner);

        if (base.IsOwner)
        {
            GameObject.FindGameObjectWithTag("PlayerFollowCamera").GetComponent<CinemachineVirtualCamera>().Follow = CinemachineCameraTarget.transform;
            _aimCamera.Follow = CinemachineCameraTarget.transform;
            
            _playerInput = GetComponent<PlayerInput>();
            _playerInput.enabled = true;

            _localInput.SetCursorState(true);
            playerNameUI.gameObject.SetActive(false);
            SetPlayerName(PlayerLobbyData.Instance.playerName);
        }
    }

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();

        _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;

        _hasAnimator = TryGetComponent(out _animator);

        AssignAnimationIDs();

        // reset our timeouts on start
        _fallTimeoutDelta = FallTimeout;
        _jumpTimeoutDelta = JumpTimeout;
    }

    [ServerRpc]
    private void SetPlayerName(string name)
    {
        playerName = name;
    }

    #endregion

    #region Update and TickUpdate

    private void TimeManager_OnTick()
    {
        if (base.IsOwner)
        {
            Reconciliation(default, false);
            CheckInput(out InputData inputData);
            ProcessInput(inputData, false);
        }
        if (base.IsServer)
        {
            ProcessInput(default, true);
            ReconcileData rd = new ReconcileData(transform.position, transform.rotation, _verticalVelocity, _fallTimeoutDelta, _jumpTimeoutDelta, Grounded, characterStates,
                _characterSkillsController.skill_1.GetDuration(), _characterSkillsController.skill_1.GetCooldown(),
                _characterSkillsController.skill_2.GetDuration(), _characterSkillsController.skill_2.GetCooldown(),
                _characterSkillsController.skill_3.GetDuration(), _characterSkillsController.skill_3.GetCooldown(),
                MoveSpeed, SprintSpeed);
            Reconciliation(rd, true);
        }
    }

    private void TimeManager_OnUpdate()
    {
        if (base.IsOwner)
        {
            //PerformSkill(ref _clientMoveData, Time.deltaTime);
            _characterSkillsController.ProcessSkillInput(_clientMoveData, Time.deltaTime);
            GroundedCheck();
            JumpAndGravity(_clientMoveData, Time.deltaTime);
            Aim(_clientMoveData, Time.deltaTime);
            Move(_clientMoveData, Time.deltaTime);
        }
    }


    private void LateUpdate()
    {
        if (base.IsOwner)
            CameraRotation();
    }

    #endregion

    [Reconcile]
    private void Reconciliation(ReconcileData rd, bool asServer)
    {
        if (Comparers.IsDefault(rd))
            return;

        transform.position = rd.Position;
        transform.rotation = rd.Rotation;
        _verticalVelocity = rd.VerticalVelocity;
        _fallTimeoutDelta = rd.FallTimeout;
        _jumpTimeoutDelta = rd.JumpTimeout;
        Grounded = rd.Grounded;
        characterStates = rd.States;
        _characterSkillsController.skill_1.SetDuration(rd.Skill_1_Duration);
        _characterSkillsController.skill_1.SetCooldown(rd.Skill_1_Cooldown);
        _characterSkillsController.skill_2.SetDuration(rd.Skill_2_Duration);
        _characterSkillsController.skill_2.SetCooldown(rd.Skill_2_Cooldown);
        _characterSkillsController.skill_3.SetDuration(rd.Skill_3_Duration);
        _characterSkillsController.skill_3.SetCooldown(rd.Skill_3_Cooldown);
        MoveSpeed = rd.MoveSpeed;
        SprintSpeed = rd.SprintSpeed;

    }    

    [Replicate]
    private void ProcessInput(InputData inputData, bool asServer, bool replaying = false)
    {
        if (asServer || replaying)
        {
            //PerformSkill(ref inputData, (float)base.TimeManager.TickDelta);
            _characterSkillsController.ProcessSkillInput(inputData, (float)base.TimeManager.TickDelta);
            GroundedCheck();
            JumpAndGravity(inputData, (float)base.TimeManager.TickDelta);
            Aim(inputData, (float)base.TimeManager.TickDelta);
            Move(inputData, (float)base.TimeManager.TickDelta);
        }
        else if (!asServer)
            _clientMoveData = inputData;
    }

    private void CheckInput(out InputData inputData)
    {
        inputData = new InputData()
        {
            Move = _localInput.move,
            Jump = _localInput.jump,
            CameraEulerY = _mainCamera.transform.eulerAngles.y,
            Sprint = _localInput.sprint,
            Aim = _localInput.aim,
            Skill_1 = _localInput.skill_1,
            Skill_2 = _localInput.skill_2,
            Skill_3 = _localInput.skill_3,
        };

        _localInput.jump = false;
    }

    private void GroundedCheck()
    {
        // set sphere position, with offset
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
            transform.position.z);
        Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
            QueryTriggerInteraction.Ignore);

        // update animator if using character
        if (_hasAnimator)
        {
            _animator.SetBool(_animIDGrounded, Grounded);
        }
    }

    private void CameraRotation()
    {
        // if there is an input and camera position is not fixed
        if (_localInput.look.sqrMagnitude >= _threshold && !LockCameraPosition)
        {
            //Don't multiply mouse input by Time.deltaTime;
            float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;

            _cinemachineTargetYaw += _localInput.look.x * deltaTimeMultiplier;
            _cinemachineTargetPitch += _localInput.look.y * deltaTimeMultiplier;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);
    }

    private void Aim(InputData inputData, float delta)
    {
        if (inputData.Aim && characterStates.isControllable == true)
        {
            _animator.SetLayerWeight(1, Mathf.Lerp(_animator.GetLayerWeight(1), 1f, delta * 20F));
            _aimCamera.gameObject.SetActive(true);
            _sensitivity = AimSensitivity;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, inputData.CameraEulerY, 0.0f), delta * 20f);
            _aiming = true;
            ChangeRigWeight(1f, delta);
            characterStates.isAiming = true;
        }
        else
        {
            _animator.SetLayerWeight(1, Mathf.Lerp(_animator.GetLayerWeight(1), 0f, delta * 20F));
            _aimCamera.gameObject.SetActive(false);
            _sensitivity = NormalSensitivity;
            _aiming = false;
            ChangeRigWeight(0f, delta);
            characterStates.isAiming = false;
        }
    }

    [ObserversRpc(RunLocally = true)]
    private void ChangeRigWeight(float targetWeight, float delta)
    {
        _rig.weight = Mathf.Lerp(_rig.weight, targetWeight, delta * 20F);
    }

    private void Move(InputData inputData, float delta)
    {
        if (!characterStates.isControllable || !characterStates.isMovable) return;

        // set target speed based on move speed, sprint speed and if sprint is pressed
        float targetSpeed = inputData.Sprint? SprintSpeed : MoveSpeed;

        if (inputData.Aim)
        {
            targetSpeed = targetSpeed * 0.7f;
            
        }

        // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

        // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is no input, set the target speed to 0
        if (inputData.Move == Vector2.zero) targetSpeed = 0.0f;

        _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, delta * SpeedChangeRate);
        if (_animationBlend < 0.01f) _animationBlend = 0f;

        // normalise input direction
        Vector3 inputDirection = new Vector3(inputData.Move.x, 0.0f, inputData.Move.y).normalized;

        // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is a move input rotate player when the player is moving
        if (inputData.Move != Vector2.zero)
        {
            _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + inputData.CameraEulerY;

            // rotate to face input direction relative to camera position
            if(!characterStates.isAiming)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, _targetRotation, 0.0f), RotationSmoothTime * delta);
            }
                
        }

        Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

        // move the player
        _controller.Move(targetDirection.normalized * (targetSpeed * delta) +
                         new Vector3(0.0f, _verticalVelocity, 0.0f) * delta);

        // update animator if using character
        if (_hasAnimator)
        {
            _animator.SetFloat(_animIDSpeed, _animationBlend);
            _animator.SetFloat(_animIDMotionSpeed, 1f);
            _animator.SetFloat(_animIDDirectionX, Mathf.Lerp(_animator.GetFloat(_animIDDirectionX), inputData.Move.x, Time.deltaTime * 10f));
            _animator.SetFloat(_animIDDirectionZ, Mathf.Lerp(_animator.GetFloat(_animIDDirectionZ), inputData.Move.y, Time.deltaTime * 10f));
        }
    }

    private void JumpAndGravity(InputData inputData, float delta)
    {
        if (Grounded)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = FallTimeout;

            // update animator if using character
            if (_hasAnimator)
            {
                _animator.SetBool(_animIDJump, false);
                _animator.SetBool(_animIDFreeFall, false);
            }

            // stop our velocity dropping infinitely when grounded
            if (_verticalVelocity < 0.0f)
            {
                _verticalVelocity = -2f;
            }

            // Jump
            if (inputData.Jump && _jumpTimeoutDelta <= 0.0f && characterStates.isControllable && characterStates.isMovable)
            {
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                // update animator if using character
                if (_hasAnimator)
                {
                    _animator.SetBool(_animIDJump, true);
                }
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
            {
                _jumpTimeoutDelta -= delta;
            }
        }
        else
        {
            // reset the jump timeout timer
            _jumpTimeoutDelta = JumpTimeout;

            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= delta;
            }
            else
            {
                // update animator if using character
                if (_hasAnimator)
                {
                    _animator.SetBool(_animIDFreeFall, true);
                }
            }
        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (_verticalVelocity < _terminalVelocity)
        {
            _verticalVelocity += Gravity * delta;
        }
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

    private void OnFootstep(AnimationEvent animationEvent)
    {
        if (!base.IsOwner) return;

        if (animationEvent.animatorClipInfo.weight > 0.5f)
        {
            if (FootstepAudioClips.Length > 0)
            {
                var index = UnityEngine.Random.Range(0, FootstepAudioClips.Length);
                AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
            }
        }
    }

    private void OnLand(AnimationEvent animationEvent)
    {
        if (!base.IsOwner) return;

        if (animationEvent.animatorClipInfo.weight > 0.5f)
        {
            AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
        }
    }

    private void OnDie()
    {
        characterStates.isAlive = false;
        characterStates.isControllable = false;
        characterStates.isMovable = false;
        _animator.applyRootMotion = true;
        _animator.SetBool(_animIDisDead, true);
    }

    
}