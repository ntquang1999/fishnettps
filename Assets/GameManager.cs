using FishNet.Connection;
using FishNet.Object;
using FishNet.Transporting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : NetworkBehaviour
{

    public bool allowConnect= false;
    public int maxPlayer = 1;
    public event Action LastPlayerLeaves;
    public int currentPlayer = 0;
    public override void OnStartServer()
    {
        base.OnStartServer();
        base.ServerManager.OnRemoteConnectionState += OnClientEvent;
    }

    private void OnClientEvent(NetworkConnection conn, RemoteConnectionStateArgs stateArg)
    {
        if (!allowConnect) conn.Disconnect(true);

        if (stateArg.ConnectionState == RemoteConnectionState.Started)
        {
            if (currentPlayer == maxPlayer) conn.Disconnect(true);
            else currentPlayer++;
        }
        else
        {
            currentPlayer--;
            if (currentPlayer == 0)
            {
                LastPlayerLeaves?.Invoke();
                Debug.Log("Last player leaves");
            }
        }
    }
}
