using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LobbyStartType
{
    Create,
    Join,
    LocalDebug
}

public enum PrototypeAstroCharacter
{
    Blue
}

public class PlayerLobbyData : MonoBehaviour
{
    public static PlayerLobbyData Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public string playerName;
    public string ServerIP;
    public ushort port;
    public LobbyStartType startType;
    public PrototypeAstroCharacter character;
}
